package Day1;

import java.util.Scanner;

public class TugasNo3 {
    public static void main(String[] args) {
        Scanner N = new Scanner(System.in);  // Scan Input
        System.out.print("Masukkan Angka : ");

        int angka = N.nextInt(); // Membaca Input
        for (int i = 0; i < angka; i++) { // Kondisi Perulangan
            System.out.printf("%3s", (i * 2)); // Output
        }
    }
}
