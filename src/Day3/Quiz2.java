package Day3;

import Day3.Pola.Angka;
import Day3.Pola.MesinPola;

import java.util.Scanner;

public class Quiz2 {
    public static void main(String[] args) {
        Program1();
    }

    private static void Program1 (){
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan Angka : ");
        int n = s.nextInt();
        Angka ma = new Angka();
        ma.setAngka(n);
        MesinPola pola = new MesinPola();
        pola.Cetak(n);
    }
}