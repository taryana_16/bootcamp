package Day3;

import Day3.Pola.MesinPola;

import java.util.Scanner;

public class PR {
    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    private int a;
    private int b;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukkan Angka : ");
        int n = sc.nextInt();
        PR pr = new PR();
        pr.pola(n);
    }

    private void pola(int n) {
        int c = 0;
        for (int i = 0; i < n; i++) {
            if (i % 4 == 2) {
                c = c + n - 1;
            } else if (i % 4 == 3) {
                c = c + n + 1;
            }
            for (int j = 0; j < n; j++) {
                if (i % 4 == 0) {
                    System.out.printf("%3d", c++);
                } else if (i % 4 == 1 && j == n - 1) {
                    System.out.printf("%3d", c++);
                } else if (i % 4 == 2) {
                    System.out.printf("%3d", c--);
                } else if (i % 4 == 3 && j == 0) {
                    System.out.printf("%3d", c++);
                } else {
                    System.out.printf("%3s", " ");
                }
            }
            System.out.println();
        }
    }
}
