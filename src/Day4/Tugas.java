package Day4;

import java.util.Scanner;

public class Tugas {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan Angka : ");
        int n = s.nextInt();
        new Tugas().pola(n);
    }

    private void pola(int n) {
        char a = 'A';
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i <= j) {
                    if (i % 2 == 1) {
                        System.out.printf("%3s", (int) a);
                    } else {
                        System.out.printf("%3s", a);
                    }
                } else {
                    System.out.printf("%3s", " ");
                }
            }
            System.out.println();
            a++;
        }
    }
}
