package Day4;

import Day3.PR;

import java.util.Scanner;

public class Latihan {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Masukkan Angka : ");
        int n = s.nextInt();
        new Latihan().pola(n);
    }

    private void pola(int n) {
        char a = 'A';
        char b = 'A';
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j || i + j == (n - 1)) {
                    System.out.print("*");
                } else if (i < j && i + j < n) {
                    System.out.print(a);
                } else if (i > j && i + j >= n) {
                    System.out.print(a);
                } else if (i < j && i + j >= n) {
                    System.out.print(--b);
                } else if (i > j && i + j < n) {
                    System.out.print(b++);
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
            if (n % 2 == 1) {
                if (i < n / 2) {
                    a++;
                } else {
                    a--;
                }
            } else {
                if (i < (n / 2) - 2) {
                    a++;
                } else if (i > (n / 2)) {
                    a--;
                }
            }
        }
        System.out.println();
    }
}
