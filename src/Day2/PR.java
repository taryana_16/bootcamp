package Day2;

public class PR {
    public static void main(String[] args) {
        int n = 9;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == i || j == (n / 2)) {
                    System.out.print((i * 2) + 1);
                } else if(i + j == (n - 1) || i == (n / 2)){
                    System.out.print((i * 2) + 1);
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
